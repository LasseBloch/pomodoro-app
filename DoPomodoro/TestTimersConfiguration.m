//
//  TestTimersConfiguration.m
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 01/03/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TimersConfiguration.h"

@interface TestTimersConfiguration : XCTestCase
@property TimersConfiguration* timerConf;
@end

@implementation TestTimersConfiguration

- (void)setUp
{
    [super setUp];
    self.timerConf = [TimersConfiguration sharedTimersConfiguration];
    [self.timerConf setTimeFor:@"taskTime" :[NSNumber numberWithInt:25 * 60]];
    [self.timerConf setTimeFor:@"shortPauseTime" :[NSNumber numberWithInt:5 * 60]];
    [self.timerConf setTimeFor:@"longPauseTime" :[NSNumber numberWithInt:15 * 60]];
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testDefaultValuesForTimers
{
    NSInteger assertedTime = 25*60;
    XCTAssertEqual([self.timerConf getTimeFor:@"taskTime"], assertedTime, @"The default task time should be 25min");
    assertedTime = 5*60;
    XCTAssertEqual([self.timerConf getTimeFor:@"shortPauseTime"], assertedTime, @"The short pause time should be 5min");
    assertedTime = 15*60;
    XCTAssertEqual([self.timerConf getTimeFor:@"longPauseTime"], assertedTime, @"The long pause time should be 15min");
}

- (void)testSetTimeForTimers
{
    NSInteger assertedTime = 2*60;
    [self.timerConf setTimeFor:@"taskTime" :[NSNumber numberWithInteger:assertedTime]];
    XCTAssertEqual([self.timerConf getTimeFor:@"taskTime"], assertedTime, @"The task time should be 2min");
    assertedTime = 3*60;
    [self.timerConf setTimeFor:@"shortPauseTime" :[NSNumber numberWithInteger:assertedTime]];
    XCTAssertEqual([self.timerConf getTimeFor:@"shortPauseTime"], assertedTime, @"The short pause time should be 3min");
    assertedTime = 4*60;
    [self.timerConf setTimeFor:@"longPauseTime" :[NSNumber numberWithInteger:assertedTime]];
    XCTAssertEqual([self.timerConf getTimeFor:@"longPauseTime"], assertedTime, @"The long pause time should be 4min");
}



@end
