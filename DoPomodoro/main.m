//
//  main.m
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 24/02/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DoPomodoroAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DoPomodoroAppDelegate class]));
    }
}
