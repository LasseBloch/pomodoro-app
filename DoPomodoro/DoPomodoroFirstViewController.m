//
//  DoPomodoroFirstViewController.m
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 24/02/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import "DoPomodoroFirstViewController.h"
#import "TimersConfiguration.h"

@interface DoPomodoroFirstViewController ()
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIButton *startStopButton;
- (IBAction)shortPause:(id)sender;
- (IBAction)longPause:(id)sender;


@property (getter = isTimerRunning) BOOL timerRunning;
@property (weak, nonatomic) NSTimer *timer;
@property (strong, nonatomic) TimersConfiguration* timerConf;
@end

@implementation DoPomodoroFirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.timerConf = [TimersConfiguration sharedTimersConfiguration];
    [self.timer fire];
    [self updateTimeLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)displayAlarmWithMessages:(NSString* )messeage
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Alert"
                              message:messeage
                              delegate:nil
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"OK",
                              nil];
    [alertView show];
}

#pragma mark -
#pragma mark setter and getters
- (NSTimer*)timer
{
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                  target:self
                                                selector:@selector(updateTimeLabel)
                                                userInfo:nil repeats:YES];
    }
    return _timer;
}

#pragma mark - update timer

- (void)updateTimeLabel
{
    NSInteger minutes = (NSInteger)(self.timerConf.timeBack / 60);
    NSInteger seconds = (NSInteger)(self.timerConf.timeBack) % 60;
    if ((minutes + seconds) > 0) {
        if (seconds < 10) {
            self.timeLabel.text = [NSString stringWithFormat:@"%ld:0%ld",(long)minutes, (long)seconds];
        }
        else {
            self.timeLabel.text = [NSString stringWithFormat:@"%ld:%ld",(long)minutes, (long)seconds];
        }
    }
    else {
        self.timeLabel.text = @"0:00";
        [self displayAlarmWithMessages:@"Time is up"];
        [self.timerConf pauseTimer];
        [self.timer invalidate];
    }
}

- (void)startStopTimer
{
    if (!self.timerRunning) {
        [self.startStopButton setTitle:@"Pause" forState:UIControlStateNormal];
        self.timerRunning = YES;
        [self.timerConf startTimer];
    }
    else {
        [self.startStopButton setTitle:@"Start" forState:UIControlStateNormal];
        self.timerRunning = NO;
        [self.timerConf pauseTimer];
    }
    
    if (!self.timer.isValid) {
        [self.timer fire];
    }
    
}

#pragma mark - ActionHandlers

- (IBAction)startPause:(UIButton *)sender {
    [self startStopTimer];
}
- (IBAction)task:(id)sender {
    if (self.timerRunning) {
        [self startStopTimer];
        [self.timerConf startTimer:@"taskTime"];
    }
    else {
        [self.timerConf selectTimer:@"taskTime"];
    }
    [self updateTimeLabel];
}

- (IBAction)shortPause:(id)sender {
    if (self.timerRunning) {
        [self startStopTimer];
        [self.timerConf startTimer:@"shortPauseTime"];
    }
    else {
        [self.timerConf selectTimer:@"shortPauseTime"];
    }
    [self updateTimeLabel];
}

- (IBAction)longPause:(id)sender {
    if (self.timerRunning) {
        [self startStopTimer];
        [self.timerConf startTimer:@"longPauseTime"];
    }
    else {
        [self.timerConf selectTimer:@"longPauseTime"];
    }
    [self updateTimeLabel];
}
@end
