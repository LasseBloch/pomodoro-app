//
//  SettingsTableViewController.h
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 06/03/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UILabel *textlabel;

@end
