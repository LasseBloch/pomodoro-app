//
//  TimersConfiguration.m
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 25/02/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import "TimersConfiguration.h"

@interface TimersConfiguration ()
@property (nonatomic, assign) NSString* selectedTimer;
@property (strong, nonatomic) NSDictionary* timers;
@property (strong, nonatomic) NSDate *startTime;
@property (strong, nonatomic) NSDate *endTime;
@property (nonatomic, assign) NSTimeInterval timeIntervalBack;
@end

@implementation TimersConfiguration

#pragma mark - Singleton Methods

+ (id)sharedTimersConfiguration
{
    static TimersConfiguration* sharedTimersConfiguration = nil;
    static dispatch_once_t onceToken;
    // Executes a block object once and only once for the lifetime of an application.
    dispatch_once(&onceToken, ^{sharedTimersConfiguration = [[self alloc] init];} );
    return sharedTimersConfiguration;
}

#pragma mark - initializing

- (id)init
{
    self = [super init];
    if (self) {
        _paused = YES;
        _areInBackground = NO;
        [self selectTimer:@"taskTime"];
    }
    return self;
}

- (NSDictionary*)timers
{
    if (!_timers) {
        _timers = @{@"taskTime": [NSNumber numberWithInt:25*60],
                    @"longPauseTime": [NSNumber numberWithInt:15*60],
                    @"shortPauseTime": [NSNumber numberWithInt:5*60]};
    }
    return _timers;
}

#pragma mark - getters and setters

// Only used for unit tests
- (BOOL)isTimeUp
{
    if ([[[NSDate date] earlierDate:self.endTime] isEqualToDate:self.endTime]) {
        return YES;
    }
    else {
        return NO;
    }
}

- (double)timeBack
{
    NSDate *now = [NSDate date];
    if (!self.paused) {
        NSTimeInterval newTimeBack = [self.endTime timeIntervalSinceDate:now];
        // If more than 0.5 secounds has passed the timer was properly paused
        // or the app may have been to the background and back
        if ( (self.timeIntervalBack - newTimeBack) > 0.5  && !self.areInBackground) {
            self.startTime = now;
            self.endTime = [[NSDate alloc] initWithTimeInterval:self.timeIntervalBack sinceDate:self.startTime];
            self.timeIntervalBack = [self.endTime timeIntervalSinceDate:now];
        }
        self.timeIntervalBack = [self.endTime timeIntervalSinceDate:now];
    }
    return self.timeIntervalBack;
}

- (NSInteger)getTimeFor:(NSString *)timer
{
    return [[self.timers valueForKey:timer] integerValue];
}

- (void)startTimer:(NSString *)timer
{
    self.startTime = [NSDate date];
    self.endTime = [[NSDate date] dateByAddingTimeInterval:(NSTimeInterval)[self getTimeFor:timer]];
    self.timeIntervalBack = [self.endTime timeIntervalSinceDate:self.startTime];
    self.selectedTimer = timer;
}

- (void)selectTimer:(NSString *)timer
{
    self.startTime = [NSDate date];
    self.endTime = [[NSDate date] dateByAddingTimeInterval:(NSTimeInterval)[self getTimeFor:timer]];
    self.timeIntervalBack = [self.endTime timeIntervalSinceDate:self.startTime];
    self.selectedTimer = timer;
}

- (void)setTimeFor:(NSString *)timer :(NSNumber *)time
{
    NSMutableDictionary *temp = [self.timers mutableCopy];
    if ([self.timers valueForKey:timer]) {
        [temp setValue:time forKey:timer];
        self.timers = [temp copy];
        if ([self.selectedTimer isEqualToString:timer] || self.selectedTimer == nil) {
            self.startTime = [NSDate date];
            self.endTime = [[NSDate date] dateByAddingTimeInterval:(NSTimeInterval)[time doubleValue]];
            self.timeIntervalBack = [self.endTime timeIntervalSinceDate:self.startTime];
        }
    }
    else {
        NSLog(@"Error tried to set value for non-existing key %@", timer);
    }
}

#pragma mark - pause unpause

- (void)pauseTimer
{
    self.paused = YES;
}

- (void)startTimer
{
    self.paused = NO;
}

@end
