//
//  DoPomodoroAppDelegate.m
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 24/02/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import "DoPomodoroAppDelegate.h"
#import "TimersConfiguration.h"

@implementation DoPomodoroAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Get ref to singleton
    NSLog(@"Hello");
    TimersConfiguration *timerConf = [TimersConfiguration sharedTimersConfiguration];
    if (!timerConf.paused) {
        NSDate *alertTime = [[NSDate date] dateByAddingTimeInterval:timerConf.timeBack];
        UIApplication *app = [UIApplication sharedApplication];
        UILocalNotification *notifyAlarm = [[UILocalNotification alloc] init];
        
        if (notifyAlarm) {
            notifyAlarm.fireDate = alertTime;
            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
            notifyAlarm.repeatInterval = 0;
            notifyAlarm.soundName = @"127879__fran-ky__100-perleggio-2.wav";
            notifyAlarm.alertBody = @"test of alarm";
            [app scheduleLocalNotification:notifyAlarm];
        }
        timerConf.areInBackground = YES;
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    TimersConfiguration *timerConf = [TimersConfiguration sharedTimersConfiguration];
    timerConf.areInBackground = NO;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
