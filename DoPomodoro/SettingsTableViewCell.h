//
//  SettingsTableViewCell.h
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 06/03/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *testLbabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

@end
