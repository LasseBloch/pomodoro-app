//
//  SettingsTableViewController.m
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 06/03/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "SettingsTableViewCell.h"
#import "TimersConfiguration.h"

@interface SettingsTableViewController ()
@property NSArray *labelTexts;
@property NSArray *timerNames;
@end

@implementation SettingsTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.labelTexts = @[@"Time for task", @"Time for short pause", @"Time for long pause"];
    self.timerNames = @[@"taskTime", @"shortPauseTime", @"longPauseTime"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"settingsCell";
    SettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    // Configure the cell...
    long section = [indexPath section];
    NSLog(@"section %ld text %@", section, self.labelTexts[section]);
    cell.testLbabel.text = self.labelTexts[section];
    cell.timeLabel.text = [self formatTimeForIndex:section];
    
    return cell;
}

- (NSString*)formatTimeForIndex:(long)index
{
    NSString *result;
    // Get ref to singleton
    TimersConfiguration *timerConf = [TimersConfiguration sharedTimersConfiguration];
    NSInteger time = [timerConf getTimeFor:self.timerNames[index]];
    
    NSInteger minutes = (time / 60);
    NSInteger seconds = (time % 60);
    if ((minutes + seconds) > 0) {
        if (seconds < 10) {
            result = [NSString stringWithFormat:@"%ld:0%ld",(long)minutes, (long)seconds];
        }
        else {
            result = [NSString stringWithFormat:@"%ld:%ld",(long)minutes, (long)seconds];
        }
    }

    return result;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
