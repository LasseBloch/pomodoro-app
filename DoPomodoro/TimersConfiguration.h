//
//  TimersConfiguration.h
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 25/02/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimersConfiguration : NSObject
@property (assign, nonatomic, readonly) NSTimeInterval timeBack;
@property BOOL areInBackground;
@property BOOL paused;

- (NSInteger)getTimeFor:(NSString*) timer;
- (void)setTimeFor:(NSString *)timer :(NSNumber *)time;
- (void)startTimer:(NSString *)timer;
- (void)selectTimer:(NSString *)timer;
- (void)pauseTimer;
- (void)startTimer;
- (BOOL)isTimeUp;


+ (id)sharedTimersConfiguration;

@end


