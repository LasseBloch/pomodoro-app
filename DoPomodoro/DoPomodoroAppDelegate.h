//
//  DoPomodoroAppDelegate.h
//  DoPomodoro
//
//  Created by Lasse Bloch Lauritsen on 24/02/14.
//  Copyright (c) 2014 Lasse Bloch's App Fabrik. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DoPomodoroAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
